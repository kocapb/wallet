<?php
/**
 * @param string $input
 * @param array $params
 * @return array
 * @throws \Exception
 */
function getData(string $input, array $params = ['code' => 4, 'wallet' => 16])
{
    if (!isset($params['code']) || !isset($params['wallet'])) {
        throw new \Exception('Wrong function parameters');
    }
    preg_match('/ \b\d{'.$params['code'].'}\b/', $input, $code);
    preg_match('/ \b\d{'.$params['wallet'].'}\b/', $input, $wallet);
    preg_match('/ \b(\d+)[\.,](\d{2})\b/', $input, $total);

    return [
        'code' => isset($code[0]) ? trim($code[0]) : null,
        'wallet' => isset($wallet[0]) ? trim($wallet[0]) : null,
        'total' => isset($total[0]) ? trim($total[0]) : null,
    ];

}